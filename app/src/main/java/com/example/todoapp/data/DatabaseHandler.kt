import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper
import com.example.todoapp.data.TodoModelClass


class DatabaseHandler(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION)
{

    companion object
    {
        private val DATABASE_VERSION = 1
        private val DATABASE_NAME = "EmployeeDatabase"

        private val TABLE_TODOS = "TodoTable"

        private val KEY_ID = "_id"
        private val KEY_TITLE = "title"
        private val KEY_ISCHECKED = "ischecked"
        private val KEY_PRIORITY = "priority"

    }

    override fun onCreate(db: SQLiteDatabase?)
    {
        val CREATE_TODOS_TABLE = ("CREATE TABLE " + TABLE_TODOS + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_TITLE + " TEXT,"
                + KEY_ISCHECKED + " INTEGER," + KEY_PRIORITY + " INTEGER " + ")")
        db?.execSQL(CREATE_TODOS_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int)
    {
        db!!.execSQL("DROP TABLE IF EXISTS $TABLE_TODOS")
        onCreate(db)
    }

    fun addTodos(todo: TodoModelClass): Long
    {
        val db = this.writableDatabase

        val contentValues = ContentValues()
        contentValues.put(KEY_TITLE, todo.title)
        contentValues.put(KEY_ISCHECKED, todo.ischecked)
        contentValues.put(KEY_PRIORITY, todo.priority)

        val success = db.insert(TABLE_TODOS, null, contentValues)

        db.close()
        return success
    }

    fun deleteTodo(id: Int):Int
    {
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(KEY_ID,id)

        val success = db.delete(TABLE_TODOS,"id-$id",null)
        db.close()
        return success
    }

    fun viewTodos(): ArrayList<TodoModelClass> {

        val empList: ArrayList<TodoModelClass> = ArrayList<TodoModelClass>()

        val selectQuery = "SELECT  * FROM $TABLE_TODOS"

        val db = this.readableDatabase

        var cursor: Cursor? = null

        try
        {
            cursor = db.rawQuery(selectQuery, null)
        }
        catch (e: SQLiteException)
        {
            db.execSQL(selectQuery)
            return ArrayList()
        }

        var id: Int
        var title: String
        var ischecked: Int
        var priority: Int

        if (cursor.moveToFirst())
        {
            do
            {
                id = cursor.getInt(cursor.getColumnIndex(KEY_ID))
                title = cursor.getString(cursor.getColumnIndex(KEY_TITLE))
                ischecked = cursor.getInt(cursor.getColumnIndex(KEY_ISCHECKED))
                priority = cursor.getInt(cursor.getColumnIndex(KEY_PRIORITY))

                val emp = TodoModelClass(id = id, title = title, ischecked = ischecked, priority = priority)
                empList.add(emp)

            } while (cursor.moveToNext())
        }
        return empList
    }
}