package com.example.todoapp

import android.graphics.Paint.STRIKE_THRU_TEXT_FLAG
import android.view.LayoutInflater
import android.view.ScrollCaptureCallback
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.todoapp.data.TodoModelClass
import kotlinx.android.synthetic.main.item_view.view.*

class TodoAdapter() : RecyclerView.Adapter<TodoAdapter.TodoViewHolder>()
{
    private var todosList: ArrayList<TodoModelClass> = ArrayList()

    class TodoViewHolder(var view: View) : RecyclerView.ViewHolder(view)
    {
        private  var title = view.findViewById<TextView>(R.id.tvTitulo)

        fun bindView(todo: TodoModelClass )
        {
            title.text = todo.title
        }
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): TodoViewHolder
    {
        return TodoViewHolder(LayoutInflater.from(p0.context).inflate(R.layout.item_view,p0,false))
    }

    fun addTodo(todo: ArrayList<TodoModelClass>)
    {
        this.todosList = todo
        notifyDataSetChanged()
    }

    fun deleteTodo(todo: ArrayList<TodoModelClass>)
    {

    }


    private fun toggleStrikeThrough(tvTitulo: TextView, isChecked: Boolean )
    {
        if(isChecked)
        {
            tvTitulo.paintFlags = tvTitulo.paintFlags or STRIKE_THRU_TEXT_FLAG
        }
        else
        {
            tvTitulo.paintFlags = tvTitulo.paintFlags and STRIKE_THRU_TEXT_FLAG.inv()
        }
    }


    override fun onBindViewHolder(p0: TodoViewHolder, p1: Int)
    {
        val curTodo = todosList[p1]
        p0.itemView.apply {
            tvTitulo.text = curTodo.title
            if(todosList[p1].ischecked == 1)
            {
                cbConcluido.isChecked = true
            }
            else if(todosList[p1].ischecked == 0)
            {
                cbConcluido.isChecked = false
            }
            toggleStrikeThrough(tvTitulo, cbConcluido.isChecked)
            if(todosList[p1].priority == 1)
            {
                imageView.setBackgroundColor(resources.getColor(android.R.color.holo_red_dark))
            }
            else if(todosList[p1].priority == 0)
            {
                imageView.setBackgroundColor(resources.getColor(android.R.color.holo_green_dark))
            }
            cbConcluido.setOnCheckedChangeListener { _, isChecked ->
                toggleStrikeThrough(tvTitulo, isChecked )
                curTodo.ischecked = 0
            }
        }
    }

    override fun getItemCount(): Int
    {
        return todosList.size
    }
}