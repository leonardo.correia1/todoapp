package com.example.todoapp


import DatabaseHandler
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.todoapp.data.TodoModelClass
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity()
{
    private lateinit var todoAdapter: TodoAdapter
    private lateinit var swt_urgent: Switch
    private lateinit var cb_done: CheckBox
    private var etxt_activity: EditText? = null

    private lateinit var sqliteHelper: DatabaseHandler
    private lateinit var recyclerView: RecyclerView
    private var adapter: TodoAdapter? = null


    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        todoAdapter = TodoAdapter()
        rv_mostrar.adapter = todoAdapter
        rv_mostrar.layoutManager = LinearLayoutManager(this)
        sqliteHelper = DatabaseHandler(this)
        this.cb_done = findViewById(R.id.cb_done)
        this.etxt_activity = findViewById(R.id.etxt_activity)
        recyclerView = findViewById(R.id.rv_mostrar)
        this.swt_urgent = findViewById<View>(R.id.swt_urgent) as Switch
        initRecyclerView()

        cb_done.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener
        {
            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean)
            {
                if(cb_done.isChecked)
                {

                }
                else
                {

                }
            }
        })
    }

    fun onClickadd(v: View)
    {
        if(swt_urgent.isChecked)
        {
            val strAct: String = this.etxt_activity?.text.toString()
            if(strAct.isNotEmpty())
            {
                val todo = TodoModelClass(0,strAct,0,1)
                sqliteHelper.addTodos(todo)
                swt_urgent.isChecked = false
                this.etxt_activity?.setText("")
            }
        }
        else
        {
            val strAct: String = this.etxt_activity?.text.toString()
            if(strAct.isNotEmpty())
            {
                val todo = TodoModelClass(0,strAct,0,0)
                sqliteHelper.addTodos(todo)
                this.etxt_activity?.setText("")
            }
        }
        val todoList = sqliteHelper.viewTodos()
        adapter?.addTodo(todoList)
    }


    fun onClickDelete(v: View)
    {

    }

    private fun initRecyclerView()
    {
        recyclerView.layoutManager = LinearLayoutManager(this)
        adapter = TodoAdapter()
        recyclerView.adapter = adapter

    }
}